from django import forms
from django.forms import ModelForm

from recipes.models import MealPlan, Rating


class RatingForm(forms.ModelForm):
    class Meta:
        model = Rating
        fields = ["value"]


class MealPlanModelForm(ModelForm):
    class Meta:
        model = MealPlan
        exclude = []


class MealPlanDeleteForm(ModelForm):
    class Meta:
        model = MealPlan
        fields = []
